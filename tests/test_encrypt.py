from cryptography.fernet import Fernet

from file_accessor.file import FakeAccessor
from file_accessor.encrypt import EncryptFileAccessor


def test_write_and_read():
    fake_file = FakeAccessor()
    file = EncryptFileAccessor(fake_file, Fernet.generate_key())
    file.write('test data')
    assert file.read() == 'test data'
