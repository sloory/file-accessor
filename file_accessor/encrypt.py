import json
from typing import AnyStr

from cryptography.fernet import Fernet
from file_accessor.file import FileAccessorInterface


class EncryptFileAccessor(FileAccessorInterface):

    def __init__(self, file_accessor: FileAccessorInterface, key: bytes):
        self.file_accessor = file_accessor
        self.encryptor = Fernet(key)

    def write(self, data: AnyStr):
        if isinstance(data, str):
            bytes_data = data.encode()
        else:
            bytes_data = data

        self.file_accessor.write(
            self.encryptor.encrypt(bytes_data).decode()
        )

    def read(self) -> str:
        return self.encryptor.decrypt(
            self.file_accessor.read().encode()
        ).decode()

    def remove(self):
        self.file_accessor.remove()

    def exists(self) -> bool:
        return self.file_accessor.exists()

    def __repr__(self):
        return json.dumps(
            {
                "path": json.loads(repr(self.file_accessor)),
            }
        )
